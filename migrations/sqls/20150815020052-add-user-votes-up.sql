CREATE TABLE "user_votes"(
  "id" SERIAL,
  "userId" INTEGER NOT NULL REFERENCES users(id),
  "voteableId" INTEGER NOT NULL REFERENCES voteables(id),
  "vote" VOTETYPE NOT NULL,
  PRIMARY KEY ("id")
)
