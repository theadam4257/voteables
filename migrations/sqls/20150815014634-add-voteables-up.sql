CREATE TABLE "voteables" (
  "id" SERIAL,
  "description" VARCHAR(100),
  "candidateA" VARCHAR(100) NOT NULL,
  "candidateB" VARCHAR(100) NOT NULL,
  PRIMARY KEY ("id")
)
