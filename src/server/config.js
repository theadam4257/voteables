
export default {
  port: 3000 || process.env.PORT,
  db: {
    url: 'postgres://localhost/dev'
  },
  jwt: {
    secret: 'V0TE 0R DI3',
    validDuration: {
      days: 7
    }
  }
};
