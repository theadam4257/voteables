import http from 'http';
import Koa from 'koa';
import bodyparser from 'koa-bodyparser';
import convert from 'koa-convert';
import Router from 'koa-66';

import {handleErrors} from './errors';
import * as User from './db/user';
import db from './db';
import apiRouter from './api';
import config from './config';

const app = new Koa();

app.on('error', (err, ctx) => {
  ctx.status = err.status || 500;
  ctx.body = {error: err};
});
app.use(handleErrors);

app.use(convert(bodyparser()));

const mainRouter = new Router();
mainRouter.mount('/api', apiRouter);

app.use(mainRouter.routes());

http.createServer(app.callback())
  .listen(config.port, () => console.log(`server started on port ${config.port}`));
