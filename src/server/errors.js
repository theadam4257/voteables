import uuid from 'node-uuid';

export default {
  ...createError('ValidationError', 400, 'Invalid'),
  ...createError('NotAuthorizedError', 401, 'NotAuthorized')
};

function createError(name, status, throwName){
  return {
    ['is' + name](error){
      return error.name === name;
    },
    ['throw' + (throwName || name)](message, overrideStatus){
      throw {
        name,
        message,
        status: overrideStatus || status,
        isCustom: true
      };
    }
  };
};

export const handleErrors = async function(ctx, next){
  try{
    await next();
  }
  catch(err){
    if(!err.isCustom){
      var id = uuid.v4();
      console.log('Server Error!!! (ID: ' + id + ')');
      console.log(JSON.stringify(err));
      console.log(err.stack);
      ctx.app.emit('error', {
        message: 'There was an error completing your request.  Please try again.',
        status: 500,
        id
      }, ctx);
    }
    else{
      ctx.app.emit('error', {
        message: err.message,
        status: err.status
      }, ctx);
    }
  }
};
