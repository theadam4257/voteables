
export function toPromiseFn(cbFn){
  return (...args) => {
    return new Promise((res, rej) => {
      cbFn(...args, (err, payload) => {
        if(err) return rej(err);
        res(payload);
      });
    });
  };
}

export function toPromise(cbFn){
  return toPromiseFn(cbFn)();
}
