import Router from 'koa-66';

import * as User from '../db/user';
import {createToken, sendTokenResponse, auth} from './token';

const userRouter = new Router();

userRouter.post('/register', async (ctx, next) => {
  const userToRegister = ctx.request.body;

  ctx.state.user = await User.create(userToRegister);

  await next();
}, sendTokenResponse);

userRouter.post('/login', async (ctx, next) => {
  const possibleUser = ctx.request.body;

  ctx.state.user = await User.authenticate(possibleUser);

  await next();
}, sendTokenResponse);

userRouter.post('/logout', auth, async (ctx) => {
  ctx.cookies.set('token');

  ct.status = 200;
});

export default userRouter;
