import Router from 'koa-66';

import userRouter from './user';

const apiRouter = new Router();

apiRouter.mount('/user', userRouter);

export default apiRouter;
