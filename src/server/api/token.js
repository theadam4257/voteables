import jwt from 'jwt-simple';
import moment from 'moment';

import config from '../config';
import errors from '../errors';
import {findByEmail} from '../db/user';

const {throwNotAuthorized} = errors;

const SUBJECT = 'sub';
const ISSUED_AT = 'iat';

export function createToken(user){
  const tokenData = {
    sub: user.email,
    iat: moment().valueOf()
  };

  return jwt.encode(tokenData, config.jwt.secret);
}

function expired(tokenData){
  return moment(tokenData.iat).isBefore(moment().subtract(config.jwt.validDuration));
}

export function decodeToken(token){
  if(!token) throwNotAuthorized('User must login first');

  const tokenData = jwt.decode(token, config.jwt.secret);

  if(expired(tokenData)) throwNotAuthorized('User session expired');

  return tokenData.sub;
}

export function sendTokenResponse(ctx){
  const token = createToken(ctx.state.user);

  ctx.cookies.set('token', token);

  ctx.status = 200;
}

export async function auth(ctx, next){
  const email = decodeToken(req.cookies.get('token'));

  const user = await findByEmail(email);

  ctx.state.user = user;

  await next();
}
