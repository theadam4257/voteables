import bcrypt from 'bcrypt';
import validator from 'validator';

import db, {now} from './';
import errors from '../errors';
import {toPromise, toPromiseFn} from '../utils';

const {throwNotAuthorized, throwInvalid} = errors;

function uniqueViolation(err){
  return err.code === '23505';
}

const promiseHash = toPromiseFn(bcrypt.hash);
const promiseSalt = toPromiseFn(bcrypt.genSalt);
const promiseCompare = toPromiseFn(bcrypt.compare);

export function checkEmail(user){
  if(!validator.isEmail(user.email)) throwInvalid('Email address is required');
  if(!validator.isLength(user.email, 1, 100)) throwInvalid('Email Address cannot be more than 100 characters long');
}

export function checkPassword(user){
  if(!validator.isLength(user.password, 1)) throwInvalid('Password must not be empty');
};

async function hashPassword(password){
  const salt = await promiseSalt(10);
  return await promiseHash(password, salt);
}

export const create = async function(user){
  checkEmail(user);
  checkPassword(user);

  const hash = await hashPassword(user.password);

  const data = {
    email: user.email.toLowerCase(),
    hash: hash,
    created: now,
    updated: now
  };

  try{
    return await toPromise(db.insert('users', data).returning('*').row);
  }
  catch(e){
    if(uniqueViolation(e)) throwInvalid('A user with this email address already exists');
    throw(e);
  }
};

export async function remove(user){
  return await toPromise(db.delete('users').where('id', user.id).returning('id').run);
}

export async function findByEmail(email){
  return await toPromise(db.select().from('users').where('email', email).row);
}

export async function authenticate(possibleUser){
  checkEmail(possibleUser);
  checkPassword(possibleUser);

  const user = await findByEmail(possibleUser.email.toLowerCase());
  const passwordsMatch = await promiseCompare(possibleUser.password, user.hash);

  if(!user || !passwordsMatch) throwNotAuthorized('Invalid username or password');

  return user;
}
