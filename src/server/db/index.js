import pgBricks from 'pg-bricks';

import config from '../config';

const db = pgBricks.configure(config.db.url);

export default db;

export const now = db.sql('now()');
