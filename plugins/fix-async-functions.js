const rewriteFunctionDeclaration = {
  FunctionDeclaration(path) {
    const t = this.t;
    const node = path.node;

    path.replaceWith(
      t.variableDeclaration(
        'const',
        [t.variableDeclarator(
          node.id,
          t.functionExpression(
            null,
            node.params,
            node.body,
            false,
            true
          )
        )]
      )
    );
  }
};

module.exports = function(babel) {
  const t = babel.types;
  return {
    visitor: {
      ExportNamedDeclaration(path) {
        if (!path.node.declaration) {
          return;
        }

        if (path.node.declaration.async !== true) {
          return;
        }

        path.traverse(rewriteFunctionDeclaration, {
          t
        });
      }
    }
  };
};
